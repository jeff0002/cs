import Vue from 'vue'
import Vuex from 'vuex'
import http from './common/http'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		userinfo : {
			nickname : 'dlq',
			image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132'
		},

		//当前选中的社区
		community : {
			id : '',
			title : '',
		},

		serviceList : [],

		URL : {
			ios : '',
		}
	},

	getters : {
		userinfo : state => state.userinfo,
		community : state => state.community,
		serviceList : state => state.serviceList,
	},

	mutations: {
		setCommunity(state, community) {
			state.community = community;
		},
		setServiceList(state, serviceList) {
			let obj = {};
			let keys = [];
			serviceList.forEach(item => {
				if(obj[item.ORG_ID] == undefined){
					obj[item.ORG_ID] = {
						text : item.type_name,
						children : [item],
					};
					keys.push(item.ORG_ID);
				}else{
					obj[item.ORG_ID].children.push(item);
				}
			});

			state.serviceList = [];
			keys.forEach(k => {
				state.serviceList.push(obj[k]);
			});
		},
		setUserInfo(state, obj) {
			state.userinfo = obj;
			state.userinfo['doc'] = obj.photo == undefined ? {} : JSON.parse(obj.photo);
		},
		setIOSURL (state, value) {
			if(state.URL.ios == ''){
				state.URL.ios = value;
			}
	    },
	},

	actions: {
		setCommunity ({ commit }, obj) {
			commit('setCommunity', obj);
		},

		getServiceList ({ commit }, obj) {
			http({
				fun : 'serviceList',
				cb : res => {
					console.log(res);
					commit('setServiceList', res);
				}
			});
		},

		getUserInfo ({ commit }, obj) {
			http({
				fun : 'userInfo',
				requestType : 'get',
				cb : res => {
					commit('setUserInfo', res);
					if(obj && obj.cb) obj.cb(res);
				}
			});
		},

	},
})
