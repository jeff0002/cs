import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import http from '@/common/http'
import util from '@/common/util'
import wx from '@/common/weixin'

import vconsole from 'vconsole'

import Vant from 'vant';
import 'vant/lib/index.css';
import '@/assets/common.css';

import { Lazyload } from 'vant';




Vue.use(Vant);
Vue.use(Lazyload, {

});

// if (document.URL.indexOf('&x=debug') > 0) {
  	var vConsole = new vconsole();
// }

Vue.config.productionTip = false

Vue.prototype.$http = http;
Vue.prototype.$util = util;
Vue.prototype.$wx = wx;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
