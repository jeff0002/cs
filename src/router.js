import Vue from 'vue'
import Router from 'vue-router'
import http from '@/common/http'
import store from './store'


import Home from './views/Home.vue'


import Main from './views/Main.vue'
import Gov from './views/gov/index.vue'
import Card from './views/card/index.vue'
import Around from './views/around/index.vue'
import Mine from './views/mine/index.vue'
import Message from './views/message/index.vue'


import UserCard from './views/usercard/index.vue'
import SearchCommunity from './views/searchCommunity/index.vue'
import SearchService from './views/searchService/index.vue'
import Event from './views/event/index.vue'
import EventInfo from './views/event/event-info.vue'
import Process from './views/process/index.vue'
import Myfile from './views/myfile/index.vue'
import CommunityInfo from './views/communityInfo/index.vue'
import Mydocument from './views/mydocument/index.vue'
import Handle from './views/handle/index.vue'

import Archives from './views/archives/index.vue'
import ArchivesImage from './views/archivesInfo/image.vue'
import ArchivesForm from './views/archivesInfo/form.vue'



import HomeInfo from './views/archivesInfo/mz/homeInfo.vue'
import BindMobile from './views/bindMobile/index.vue'



Vue.use(Router)

let r = new Router({
    routes: [
        {
            path: '/',
            name: 'main',
            component: Main,
            children : [
                {
                    name : 'gov',
                    path: 'gov',
                    component: Gov,
                    meta: {
                        title:"首页",
                    }
                },
                {
                    name : 'card',
                    path: 'card',
                    component: Card,
                    meta: {
                        title:"便民卡",
                    }
                },
                {
                    name : 'around',
                    path: 'around',
                    component: Around,
                    meta: {
                        title:"周边",
                    }
                },
                {
                    name : 'mine',
                    path: 'mine',
                    component: Mine,
                    meta: {
                        title:"我的",
                    }
                },
            ]
        },

        {
            name: 'usercard',
            path: '/usercard',
            component: UserCard,
        },

        {
            name: 'searchCommunity',
            path: '/searchCommunity',
            component: SearchCommunity,
        },

        {
            name: 'searchService',
            path: '/searchService',
            component: SearchService,
            meta: {
                title : "服务检索",
                keepAlive : true,
            }
            
        }, 
        {
            name: 'message',
            path: '/message',
            component: Message,
            meta: {
                title:"消息",
            }
        },
        {
            name: 'event',
            path: '/event',
            component: Event,
            meta: {
                title:"我办理的事项",
            }
        },
        {
            name: 'eventInfo',
            path: '/eventInfo',
            component: EventInfo,
            meta: {
                title:"事项详情",
            }
        },
        {
            name: 'process',
            path: '/process',
            component: Process,
            meta: {
                title:"办理流程",
            }
        },
        {
            name: 'myfile',
            path: '/myfile',
            component: Myfile,
            meta: {
                title:"我的文件",
            }
        },
        {
            name: 'communityInfo',
            path: '/communityInfo',
            component: CommunityInfo,
            meta: {
                title:"组织架构",
            }
        },
        
        {
            name: 'mydocument',
            path: '/mydocument',
            component: Mydocument,
            meta: {
                title:"自助办理",
            }
        },

        
        {
            name: 'handle',
            path: '/handle',
            component: Handle,
            meta: {
                title:"办理",
            }
        },

        {
            name: 'archives',
            path: '/archives',
            component: Archives,
            meta: {
                title:"我的档案",
            }
        },
        {
            name: 'archivesImage',
            path: '/archivesImage',
            component: ArchivesImage,
            meta: {
                title:"档案详情",
            }
        },
        {
            name: 'archivesForm',
            path: '/archivesForm',
            component: ArchivesForm,
            meta: {
                title:"档案详情",
            }
        },

        {
            name: 'homeInfo',
            path: '/homeInfo',
            component: HomeInfo,
            meta: {
                title:"家庭信息",
            }
        },

        {
            name: 'bindMobile',
            path: '/bindMobile',
            component: BindMobile,
            meta: {
                title:"绑定电话",
            }
        },


        {
          path: '/about',
          name: 'about',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }
    ],

    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
    
});


const debug = process.env.NODE_ENV !== 'production'

r.beforeEach((to, from, next) => {

    // console.log(debug);

    if (to.meta.title) {
        document.title = to.meta.title;
    }

    var u = navigator.userAgent;
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

    if (isiOS) {
        store.commit('setIOSURL', window.location.href);
    }

    //不走授权页面
    if(to.name == 'gov' 
    || to.name == 'card'
    || to.name == 'around'
    // || to.name == 'handle'
    ){
        next();
        return;
    }


    if (debug) {
        //测试
        http({
            fun : 'mnlogin',
            requestType : 'get',
            cb : res => {
                store.dispatch('getUserInfo', {
                    cb : res => {
                        next();
                    }
                });
            }
        });
    }else{

        http({
            fun : 'checkneedauth',
            requestType : 'get',
            cb : res => {
                console.log(res);
                if(res == '0'){ //不需要授权
                    store.dispatch('getUserInfo', {
                        cb : res => {
                            next();
                        }
                    });
                }else{  //需要授权
                    next(false);

                    window.location.href = 'http://csapi.tripln.com/v1/un/auth?rurl='+ 
                    encodeURIComponent('https://cs.tripln.com/#' + to.fullPath);

                    return;
                }
            }
        });
    }

});



export default r;
