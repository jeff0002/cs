const gov = {
	toppic : [
		{url : "https://img.hidlq.com/image/community/gov/2.jpeg"},
		{url : "https://img.hidlq.com/image/community/gov/1.jpg"},
	],

	articleList : [
		{
			id : 1,
			title : '辽宁8890政务便民服务平台上线 沈阳成首批开通城市',
			date : '2019-02-12',
			content : '近日，《辽宁省8890政务便民服务平台建设实施方案》正式公布，全省将建设统一的8890政务便民服务平台，不论在哪个市，只需要拨打8890（0）000这一个电话，就可以进行投诉、咨询或者求助。',
			image : 'https://img.hidlq.com/image/community/gov/1.jpg',
		},
		{
			id : 2,
			title : '辽宁取消调整253项证明，75项证明将被永久取消',
			date : '2019-02-13',
			content : '近日，省政府公布《辽宁省第一批取消调整涉及企业和群众办事创业的证明目录》，共有253项证明取消调整。其中，永久性取消的证明有75项，取消后规范调整办理方式的证明有178项。',
			image : 'https://img.hidlq.com/image/community/gov/2.jpeg',
		}
	],

	service : [
		{
			title : '食品安全',
			img : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			articlelist : [
				{title : '群众食品安全问题举报受理', id : '1'},
				{title : '2222', id : '2'},
				{title : '3333', id : '3'},
			],
		},
		{
			title : '残联',
			img : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			articlelist : [
				{title : '办理《残疾人证》', id : '1'},
				{title : '申请办理《残疾人证》内容', id : '2'},
				{title : '3333', id : '3'},
			],
		},
		{
			title : '民政',
			img : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			articlelist : [
				{title : '申请居民低保收入核对', id : '1'},
				{title : '申请办理城镇居民最低生活保障', id : '2'},
				{title : '3333', id : '3'},
			],
		}

	],
};


const communityList = [
	{
		id : 1,
		title : '沈河区风雨坛社区11',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		mobile : '88888888',
		address : '沈河区北热闹路60-2号'
	},
	{
		id : 2,
		title : '沈河区风雨坛社区22',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		mobile : '88888888',
		address : '沈河区北热闹路60-2号'
	},
	{
		id : 3,
		title : '沈河区风雨坛社区33',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		mobile : '88888888',
		address : '沈河区北热闹路60-2号'
	},
];


const card = {
	toppic : [
		{url : "https://img.hidlq.com/image/LW00035/shopconfig/1548817772048jaO94g02Hw5rvkCZ.jpg"},
		{url : "https://img.hidlq.com/image/LW00035/shopconfig/1548817777616Ii8pU8yuiY2JjPYU.jpg"},
		{url : "https://img.hidlq.com/image/LW00035/shopconfig/1548817782915l1HZ2X4B0MmubQLJ.jpg"},
	],

	notice : '社区便民卡数据采集启动，持身份证居住证可办理。社区便民卡数据采集启动，持身份证居住证可办理。',

	productList : [
		{
			id : 1,
			image : 'https://img.hidlq.com/image/community/gov/p1.png',
			title : '测试产品11',
			price : 100,
		},
		{
			id : 2,
			image : 'https://img.hidlq.com/image/community/gov/p2.png',
			title : '测试产品222',
			price : 130,
		},
		{
			id : 3,
			image : 'https://img.hidlq.com/image/community/gov/p3.png',
			title : '产品名称特别特别长，就是很长长长',
			price : 230,
		},
		{
			id : 4,
			image : 'https://img.hidlq.com/image/community/gov/p4.png',
			title : '测试产品2xxxx',
			price : 130.31,
		},
	],

	marryList : [
		{
			id : 1,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '张先森',
			age : 23,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '海归', 'tag' : 'danger'},
				{'label' : '海归', 'tag' : 'danger'},
				{'label' : '海归', 'tag' : 'danger'},
			],
		},
		{
			id : 2,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '王先森',
			age : 43,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '王者', 'tag' : 'primary'},
			],
		},
		{
			id : 3,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '张先森',
			age : 23,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '青铜', 'tag' : ''},
			],
		},
		{
			id : 4,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '李先森',
			age : 23,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '海归', 'tag' : 'danger'},
			],
		},
		{
			id : 5,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '张先森',
			age : 23,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '海归', 'tag' : 'danger'},
			],
		},
		{
			id : 6,
			image : 'https://img.hidlq.com/image/community/gov/h1.png',
			name : '李先森',
			age : 23,
			area : '朱剪炉社区',
			remark : [
				{'label' : '硕士', 'tag' : 'success'},
				{'label' : '海归', 'tag' : 'danger'},
			],
		},
	],

	travelList : [
		{
			id : 1,
			title : 'XXX温泉免费游',
			image : 'https://img.hidlq.com/image/community/gov/g2.png',
			price : 0,
			market : 116,
			percent : 35,

		},
		{
			id : 2,
			title : 'XXX滑雪免费游',
			image : 'https://img.hidlq.com/image/community/gov/g1.png',
			price : 0,
			market : 316,
			percent : 65,
			
		},
	],

	killList : [
		{
			id : 1,
			title : 'XXXX滑雪免费游',
			image : 'https://img.hidlq.com/image/community/gov/g1.png',
			tag : '活动进行中',
			minprice : 0,
			maxprice : 128,
			stock : 32,
		},
		{
			id : 2,
			title : 'XXXX滑雪免费游111',
			image : 'https://img.hidlq.com/image/community/gov/g2.png',
			tag : '活动进行中',
			minprice : 0,
			maxprice : 1228,
			stock : 322,
		},
	],
};


const around = {

	welfare : [
		{
			id : 1,
			title : '公益游活动火热报名中',
			image : 'https://img.hidlq.com/image/community/gov/g3.png',
		},
	],

	productList : [
		{
			id : 1,
			image : 'https://img.hidlq.com/image/community/gov/p1.png',
			title : '测试产品11',
			price : 100,
		},
		{
			id : 2,
			image : 'https://img.hidlq.com/image/community/gov/p2.png',
			title : '测试产品222',
			price : 130,
		},
		{
			id : 3,
			image : 'https://img.hidlq.com/image/community/gov/p3.png',
			title : '产品名称特别特别长，就是很长长长',
			price : 230,
		},
		{
			id : 4,
			image : 'https://img.hidlq.com/image/community/gov/p4.png',
			title : '测试产品2xxxx',
			price : 130.31,
		},
	],

	property : [
		{
			id : 1,
			title : '开锁',
			image : 'https://img.hidlq.com/image/community/gov/y1.png',
			content : '公安局备案，多年开锁经验',
			price : '开锁100元起',
		},
		{
			id : 2,
			title : '排油烟机清洗',
			image : 'https://img.hidlq.com/image/community/gov/y2.png',
			content : '专业人员上门清洗油烟机',
			price : '100元起',
		},
		{
			id : 3,
			title : '清洗地热',
			image : 'https://img.hidlq.com/image/community/gov/y3.png',
			content : '专业设备清洗地热',
			price : '200元起',
		},
	],

};

const serviceList = [
	{
		text: '民政',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		children: [
			{
				text: '居民低保收入核对居民低保收入核对居民低保收入核对居民低保收入核对居民低保收入核对',
				id: 1001,
				
			},
			{
				text: '城镇居民最低生活保障',
				id: 1002
			},
			{
				text: '城镇居民最低生活保障调标',
				id: 1003,
				disabled: true,
			},
			{
				text: '城镇居民最低生活保障停保',
				id: 1004
			},
		]
	},
	{
		text: '红十字会',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		children: [
			{
				text: '居民低保收入核对',
				id: 2001,
				disabled: true,
			},
			{
				text: '城镇居民最低生活保障',
				id: 2002
			},
			{
				text: '城镇居民最低生活保障调标',
				id: 2003
			},
			{
				text: '城镇居民最低生活保障停保',
				id: 2004
			},
		]
	},
	{
		text: '医保',
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
		children: [
			{
				text: '居民低保收入核对',
				id: 3001,
			},
			{
				text: '城镇居民最低生活保障',
				id: 3002
			},
			{
				text: '城镇居民最低生活保障调标',
				id: 3003
			},
			{
				text: '城镇居民最低生活保障停保',
				id: 3004
			},
		]
	},

];

//state:0,草稿，1：待审核，2：受理中，3:已完结
const myEventList = [
	{
		id : 1,
		title : '低保户，低保边缘户身份证证明出具11',
		date : '2019-01-21',
		state : 0,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 2,
		title : '低保户，低保边缘户身份证证明出具22',
		date : '2019-01-21',
		state : 2,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 3,
		title : '低保户，低保边缘户身份证证明出具33',
		date : '2019-01-21',
		state : 3,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 4,
		title : '低保户，低保边缘户身份证证明出具44',
		date : '2019-01-21',
		state : 1,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 5,
		title : '低保户，低保边缘户身份证证明出具55',
		date : '2019-01-21',
		state : 1,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 6,
		title : '低保户，低保边缘户身份证证明出具66',
		date : '2019-01-21',
		state : 1,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},
	{
		id : 7,
		title : '低保户，低保边缘户身份证证明出具77低保户，低保边缘户身份证证明出具77低保户，低保边缘户身份证证明出具77',
		date : '2019-01-21',
		state : 0,
		company : '沈河区风雨坛社区',
		acceptCommpany : '沈阳市民政局',
	},


];




const serviceObj = {
	'1001' : {
		title : '居民低保收入核对',

		// ================= 办理流程数据 ============================ //

		//受理条件
		acceptingConditions : '低保户，低保边缘户',
		//所需条件
		needConditions : [
			{
				content : '低保证、低保边缘证原件及关键信息页复印件',
				example : 1,
			},
			{
				content : '低保证、低保边缘证原件及关键信息页复印件1212',
				example : 1,
			},
			{
				content : '低保证、低保边缘证原件及关键信息页复印件31231',
				example : 1,
			},
		],
		//流程图 
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
	
		// ================= 办理流程数据 ============================ //



		// ================= 要件数据 ============================ //
		hasImpDoc : true,
		impDoc : [
			{
				content : '手持身份证拍照，照片完整显示证件号码',
				image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			},
			{
				content : '手持身份证正面复印件',
				image : '',
			},
			{
				content : '手持身份证反面复印件',
				image : '',
			},
		],
		hasForm : true,
		formData : [
			{
				type : 'text',
				label : '姓名',
				name : 'username',
				value : '',
			},
			{
				type : 'text',
				label : '身份证',
				name : 'cardno',
				value : '',
			},
		],
		// ================= 要件数据 ============================ //
		
	},
	'1002' : {
		title : '城镇居民最低生活保障',
		//受理条件
		acceptingConditions : '22低保户，低保边缘户',
		//所需条件
		needConditions : [
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件',
				example : 1,
			},
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件1212',
				example : 1,
			},
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件31231',
				example : 1,
			},
		],
		//流程图 
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
	
		// ================= 要件数据 ============================ //
		hasImpDoc : true,
		impDoc : [
			{
				content : '22手持身份证拍照，照片完整显示证件号码',
				image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			},
			{
				content : '22手持身份证正面复印件',
				image : '',
			},
			{
				content : '22手持身份证反面复印件',
				image : '',
			},
		],
		hasForm : false,
		formData : [
			{
				type : 'text',
				label : '姓名',
				name : 'username',
				value : '',
			},
			{
				type : 'text',
				label : '身份证',
				name : 'cardno',
				value : '',
			},
		],
		// ================= 要件数据 ============================ //
	},

	'1004' : {
		title : '城镇居民最低生活保障',
		//受理条件
		acceptingConditions : '22低保户，低保边缘户',
		//所需条件
		needConditions : [
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件',
				example : 1,
			},
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件1212',
				example : 1,
			},
			{
				content : '2低保证、低保边缘证原件及关键信息页复印件31231',
				example : 1,
			},
		],
		//流程图 
		image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
	
		// ================= 要件数据 ============================ //
		hasImpDoc : false,
		impDoc : [
			{
				content : '22手持身份证拍照，照片完整显示证件号码',
				image : 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIWd4ibJ7fygdZeYq0KmrNFNkzTngiae8hGE3XVjPMQKjibQJIXagMcrqewKskbs6r8DiapXibNJVIwqyQ/132',
			},
			{
				content : '22手持身份证正面复印件',
				image : '',
			},
			{
				content : '22手持身份证反面复印件',
				image : '',
			},
		],
		hasForm : true,
		formData : [
			{
				type : 'text',
				label : '姓名',
				name : 'username',
				value : '',
			},
			{
				type : 'text',
				label : '身份证',
				name : 'cardno',
				value : '',
			},
		],
		// ================= 要件数据 ============================ //
	},
}


export default {
	gov,
	communityList,
	card,
	around,
	serviceList,
	myEventList,
	serviceObj,
}