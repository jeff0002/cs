import wx from 'weixin-js-sdk'
import http from '@/common/http'
import util from '@/common/util'
import store from '@/store'
import Vue from 'vue'
import { Toast } from 'vant';

Vue.use(Toast);


function _init (cb) {

	Toast.loading({
        duration: 0,       // 持续展示 toast
        forbidClick: true, // 禁用背景点击
        loadingType: 'spinner',
        message: '请稍后...'
    });

	let dlqurl = '';

	var u = navigator.userAgent;
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

    if (isiOS) {
        dlqurl = store.state.URL.ios;
    }else{
    	dlqurl = window.location.href;
    }

    console.log(dlqurl);

	http({
		fun : 'wxConfigInfo',
		url : dlqurl,
		cb : r => {
			console.log(r);

			wx.config({
			    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			    appId: r.appid, // 必填，公众号的唯一标识
			    timestamp: r.timestamp, // 必填，生成签名的时间戳
			    nonceStr: r.noncestr, // 必填，生成签名的随机串
			    signature: r.signature, // 必填，签名，见附录1
			    //jsApiList: ['chooseWXPay'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				jsApiList: ['showMenuItems','hideAllNonBaseMenuItem','getLocalImgData','downloadImage','chooseWXPay', 
				'chooseImage', 'onMenuShareAppMessage', 'onMenuShareTimeline', 'uploadImage', 'previewImage', 'getLocation', 
				'openLocation','hideMenuItems', 'scanQRCode']
			});

			wx.ready(function(){
				Toast.clear();
				cb();
			});

			wx.error(res => {
				console.log('wx.error');
	    		console.log(res);
	    		Toast.clear();
			});
		}
	});

}

function showImage (cb) {

	_init (_ => {

		console.log('kais');
		wx.chooseImage({
			count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success: function (res) {
				console.log('chooseImage');
				console.log(res);
				var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
				wx.getLocalImgData({
					localId: localIds[0], // 图片的localID
					success: function (res) {

						Toast.loading({
		                    duration: 0,       // 持续展示 toast
		                    forbidClick: true, // 禁用背景点击
		                    loadingType: 'spinner',
		                    message: '上传中...'
		                });

						var localData = res.localData; // localData是图片的base64数据，可以用img标签显示
						if (localData.indexOf('data:image') != 0) {
							localData = 'data:image/jpeg;base64,' + localData;
						}

						let fd = new FormData();
						fd.append('dir', 'community/');

						var fileName = new Date().getTime() + ".jpeg";
						fd.append('file', util.dataURLtoFile(localData, fileName));

						http({
							fun : 'upload',
							requestImage : true,
							fd,
							cb : res => {
								console.log('上传图片成功');
								console.log(res);
								Toast.clear();
								if(cb) cb(res);
							},
							err : res => {
								Toast.clear();
								Toast('上传失败 : ' + res);
							}
						});
					}
				});
			}
		});

	});

}



function checkUserCard (cb) {

	_init (_ => {

		wx.chooseImage({
			count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success: function (res) {
				console.log('chooseImage');
				console.log(res);
				var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片

				wx.uploadImage({
					localId: localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
					isShowProgressTips: 1, // 默认为1，显示进度提示
					success: function (res) {
						var serverId = res.serverId; // 返回图片的服务器端ID
						console.log('返回图片的服务器端ID');
						console.log(serverId);
						
						http({
							fun : 'yunMaiOrc',
							mediaid : serverId,
							cb : res => {
								if(cb) cb(res);
							}
						});
					}
				});
			}
		});

	});

}

export default {
	showImage,
	checkUserCard,
}