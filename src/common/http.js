const axios = require('axios');
import api from './api'
import mock from './mock.data'

function mockdata (fun, cb) {
	cb(mock[fun]);
}

export default ({ mock, fun, cb, err, final, requestType, requestImage, liangzong, ...data }) =>  {

	if(mock){
		mockdata(fun, cb);
		return;
	}

	const url = api[fun];

	//默认post请求。
	const method = requestType == 'get' ? 'GET' : 'POST';

	//把空属性去掉。
	Object.keys(data).forEach(key => {
		if(data[key] === undefined){
			delete data[key];
		}
	});

	data['company_code'] = 'LW12345';

	const config = {
		method,
		url,

	};

	if(method == 'GET'){
		config['params'] = data;
	}else if(method == 'POST'){
		//上传图片
		if(requestImage){
			config['headers'] = {
				'Content-Type' : 'multipart/form-data',
			};
			config['data'] = data.fd;
		}else{
			config['data'] = data;
		}
	}
	// console.log(config);

	axios(config)
	.then(res => {

		if(liangzong){
			console.log(res.data);
			if(cb) cb(res.data);
			return;
		}

		if(res.data.errcode == 0){
			console.log(res.data);
			if(cb) cb(res.data.data);
		}else{
			console.log(url);
			console.log(res.data.errmsg);
			if(err) err(res.data.errmsg);
		}
	})
	.catch(res => {
		console.log('http 中的异常');
		console.log(url);
		console.log(res);
	})

}