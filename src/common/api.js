

const api =  {
	
	serviceList : '/api/us/csc/audititem/list',

	serviceInfo : '/api/uc/csc/auditItemService/itemInfo',

	//userInfo : '/api/uc/wc/userWxSkService/memberlevel',
	//userInfo : '/v1/ua/getMemberInfo',
	//用户信息
	userInfo : '/api/us/csc/user/getuserinfo1',

	//模拟登录
	mnlogin : '/v1/un/mnlogin',

	//检测是否需要授权
	checkneedauth : '/v1/un/checkneedauth',
	// checkneedauth : '/Api/Api/is_redis_company',
	// justurl : '/Api/Api/skForeAuth/pcid/',

	//图片上传
	upload : '/v1/pn/upload',

	//调微信签名
	wxConfigInfo : '/v1/wn/configinfo',

	//云脉身份证验证
	yunMaiOrc : 'https://txy.sylyx.cn/Api/Api/yunMaiOrc',

	//更新身份证信息
	updateCardNo : '/api/uc/csc/userRegisterService/updateCardNo',

	//更新电话
	updateMobile : '/api/uc/csc/userRegisterService/updateMobile',

	//档案列表
	archiveslist : '/api/us/csc/audititem/archiveslist',

	//通过档案信息获取   pid
	archivesInfoByPid : '/api/us/csc/audititem/getdocumentbypidlist',

	//更改要件
	updateDoc : '/api/uc/csc/userRegisterService/updatePhoto',

	//下单草稿 state == '' 时调用
	createOrder : '/api/uc/csc/auditOrderService/createOrderByUser',

	//修改草稿 state == '9' 时调用
	updateOrderInfo : '/api/us/cdc/auditorder/updateOrderByUser',

	//提交审核
	updateOrder : '/api/uc/csc/auditOrderService/updateOrder',

	//订单详情
	orderInfo : '/api/us/csc/auditorder/getorderbyuid',

	//订单列表
	orderList : '/api/us/csc/auditorder/wxorderlist',

	//要件详情  item_id
	documentInfo : '/api/us/csc/audititem/documentlist',

	//要件提报
	setOrderTB :'/api/uc/csc/auditOrderService/setOrderTB',


	// -------------- 对接易迅 -------------------------//
	//证件类型
	docType : '/api/uc/csc/createPostService/getAppraising',
	//区别
	qu : '/api/uc/csc/createPostService/getAdministrativePlanning',
	//回显
	huixian : '/api/uc/csc/createPostService/getBack',
	// -------------- 对接易迅 -------------------------//


	sendMessage : '/api/uc/csc/sendMessageService/sendMessage',
	
	checkMessage : '/api/uc/csc/sendMessageService/checkMessage',


};


export default api;