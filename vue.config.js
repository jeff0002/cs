module.exports = {
  	lintOnSave: false,

	devServer: {
        proxy: {
        	'/Api/Api/' : {
				ws: false,
				target: 'https://txy.sylyx.cn',
				//target: 'http://t.hidlq.com',
				changeOrigin: true,
				secure: false,
			},
			'/v1/' : {
				ws: false,
				target: 'http://csapi.tripln.com',
				//target: 'http://t.hidlq.com',
				changeOrigin: true,
				secure: false,
			},
			'/api/' : {
				ws: false,
				target: 'https://cs.tripln.com',
				//target: 'http://t.hidlq.com',
				changeOrigin: true,
				secure: false,
			},

        }
    },

}